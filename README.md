# LAS 2019 GTK/GStreamer in Rust Workshop

## Setup

Install the Rust toolchain via `rustup`

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Check https://rustup.rs for alternative installation options.

### Ubuntu/Debian/etc

```sh
apt install libgtk-3-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
```

### Fedora/RedHat/SuSE/etc

```sh
dnf install gtk3-devel gstreamer1-devel gstreamer-plugins-base1-devel
```

## Documentation

Documentation for Rust is available at:
 * The Rust Book: https://doc.rust-lang.org/book
 * Rust by Example: https://doc.rust-lang.org/stable/rust-by-example/
 * Standard library API reference: https://doc.rust-lang.org/std/index.html

API Documentation for GTK and GStreamer Rust bindings is available at:

 * https://gtk-rs.org/docs/gtk
 * https://slomo.pages.freedesktop.org/rustdocs/gstreamer/gstreamer/

Example code for GTK and GStreamer in Rust is available at:

 * https://github.com/gtk-rs/examples/
 * https://gitlab.freedesktop.org/gstreamer/gstreamer-rs/tree/0.14/examples

## Workshop Steps

All steps of the workshop are available in the `chapter-1`, `chapter-2`, etc.
tags of this repository. The `master` branch contains the final version and
potentially some changes on top of that.

## Slides

The slides of the workshop are available in the `slides` branch of this
repository.
